var searchData=
[
  ['is_5fbasic_5funit',['is_Basic_Unit',['../structomni_1_1is__Basic__Unit.html',1,'omni']]],
  ['is_5fbasic_5funit_3c_20basic_5funit_3c_20dimension_2c_20rep_2c_20period_2c_20origin_20_3e_20_3e',['is_Basic_Unit&lt; Basic_Unit&lt; Dimension, Rep, Period, Origin &gt; &gt;',['../structomni_1_1is__Basic__Unit_3_01Basic__Unit_3_01Dimension_00_01Rep_00_01Period_00_01Origin_01_4_01_4.html',1,'omni']]],
  ['is_5fcomplete_5funit',['is_Complete_Unit',['../structomni_1_1is__Complete__Unit.html',1,'omni']]],
  ['is_5fcomplete_5funit_3c_20complete_5funit_3c_20dimension_2c_20rep_2c_20period_2c_20origin_20_3e_20_3e',['is_Complete_Unit&lt; Complete_Unit&lt; Dimension, Rep, Period, Origin &gt; &gt;',['../structomni_1_1is__Complete__Unit_3_01Complete__Unit_3_01Dimension_00_01Rep_00_01Period_00_01Origin_01_4_01_4.html',1,'omni']]],
  ['is_5fdimension',['is_Dimension',['../structomni_1_1is__Dimension.html',1,'omni']]],
  ['is_5fdimension_3c_20dimension_3c_20length_2c_20mass_2c_20time_2c_20current_2c_20temperature_2c_20quantity_2c_20luminous_5fintensity_20_3e_20_3e',['is_Dimension&lt; Dimension&lt; length, mass, time, current, temperature, quantity, luminous_intensity &gt; &gt;',['../structomni_1_1is__Dimension_3_01Dimension_3_01length_00_01mass_00_01time_00_01current_00_01tempe72b48c02bb0b63507c29e245fbf49005.html',1,'omni']]],
  ['is_5fstb_5fratio',['is_stb_Ratio',['../structomni_1_1is__stb__Ratio.html',1,'omni']]],
  ['is_5fstb_5fratio_3c_20ratio_3c_20num_2c_20den_20_3e_20_3e',['is_stb_Ratio&lt; Ratio&lt; Num, Den &gt; &gt;',['../structomni_1_1is__stb__Ratio_3_01Ratio_3_01Num_00_01Den_01_4_01_4.html',1,'omni']]],
  ['is_5fstd_5fratio',['is_std_Ratio',['../structomni_1_1is__std__Ratio.html',1,'omni']]],
  ['is_5fstd_5fratio_3c_20std_3a_3aratio_3c_20num_2c_20den_20_3e_20_3e',['is_std_Ratio&lt; std::ratio&lt; Num, Den &gt; &gt;',['../structomni_1_1is__std__Ratio_3_01std_1_1ratio_3_01Num_00_01Den_01_4_01_4.html',1,'omni']]]
];
